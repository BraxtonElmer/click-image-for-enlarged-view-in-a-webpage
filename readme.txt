This Repository has the code for an image when clicked in a webpage it enlarges giving a bigger view.
The code is written in HTML and CSS

When you open the index.html file in your browser, an image appears when you click the image the image enlarges with a traansition effect giving a better view, to exit the enlarged view select the X button on the top right.

This repository has an example image with it, you can change the image's name or directory, but make sure you make the changes in your index.html file also.

For example: you are changing the image's name to 'test-image', make sure you change the code from the index.html in the 10th line from <img src="image.jpg"> to <img src="test-image.jpg"> and change the code in the 13th line from <img src="image.jpg"> to <img src="test-image.jpg">

If your are changing the directory of the image, for example you have an image named test-image and you put it in a folder named images then change the code from the index.html file in the 10th line from <img src="image.jpg"> to <img src="images/test-image.jpg"> and the same for your 13th line.

Incase you have the images folder in the root directory then you can change the code from <img src="image.jpg"> to <img src="/images/test-image.jpg">

You can use any image in the file format- .jpg | .jpeg | .png | .svg | .gif | but make sure to change the code in the index.html file from <img src="image.'your-extension'"> 
For example, if you use a .png image then the code must <img src="image.png">, if it was gif, the code is <img src="image.gif">

You don't have to change anything in the style.css file

This repository is free to use on any webpage whether personal or business!

For more codes and doubts on other programs join our discord: http://braxtonelmer.com/join-discord/

Contact me at contact.braxtonelmer@gmail.com if you have any issues with the code
